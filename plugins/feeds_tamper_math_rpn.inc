<?php

/*
 * Requires REAR RPN library.
 * 
 * Get it first http://pear.php.net/package/Math_RPN/download
 * 
 *  or simply do:
 *  
 *    `pear install Math_RPN-1.1.2`
 * 
 */
require_once 'Math/RPN.php';

$plugin = array(
  'form'     => 'feeds_tamper_math_form',
  'callback' => 'feeds_tamper_math_callback',
  'name'     => 'Math RPN',
  'multi'    => 'skip',
  'category' => 'Number',
);

function feeds_tamper_math_form($importer, $element_key, $settings) {
  $form = array();
  $mappings = $importer->processor->config['mappings'];
  $replace = array();

  foreach ($mappings as $mapping) {
    $replace[] = '[' . $mapping['source'] . ']';
  }

  $form['text'] = array(
    '#type' => 'textarea',
    '#title' => t('Replacement pattern'),
    '#default_value' => isset($settings['text']) ? $settings['text'] : '',
  );
  $form['help'] = array(
    '#type' => 'fieldset',
    '#title' => t('Available Replacement Patterns'),
    '#collapsed' => TRUE,
    '#collapsible' => TRUE,
    '#value' => theme('item_list', array('items' => $replace)),
  );
  return $form;
}

function feeds_tamper_math_callback($result, $item_key, $element_key, &$field, $settings) {
  static $rpn;
  if (!isset($rpn)) {
    $rpn = new Math_Rpn(); 
  }

  $trans = array();
  $item = $result->items[$item_key];
  foreach ($item as $key => $value) {
    $trans['[' . $key . ']'] = $value;
  }
  $expression = strtr($settings['text'], $trans); 
  if (($ret = $rpn->calculate($expression, 'deg', true)) != 'Syntax error') {
    $field = $ret; 
  }
}
