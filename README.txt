* Feeds Tampler Match RPN plugin

This is a plugin for making arithmetical calculations when tampering imported data with Feeds Tamper module.

* Installation

This module depends on PEAR's Math_RPN package which is pretty fast calculator. So you need to grab it first:

  http://pear.php.net/package/Math_RPN/download

On compliant systems this is done by issuing:

  $ pear install Math_RPN-1.1.2

Then enable the module and when you're done, additional tampering rule "Math RPN" will appear under "Number" category.
Note that your expressions must be in RPN-format (http://en.wikipedia.org/wiki/Reverse_Polish_notation), so instead of writing:

[price] * 0.9 + 100

you must use the form:

[price] 0.9 * 100 +

* Backgruond

Why RPN? Because when we import, we care about speed. I know Feeds Import itself is not that fast procedure, but I didn't want to add 
more slowness to this. And the Math_RPN calculator is fast.
You are welcome to review results of a simple performance test http://pastebin.com/XCqkwAKW:

Running 1000 times
Using PHP native: 0.00071191787719727s
Using PHP eval: 0.0074231624603271s
Using PEAR RPN true-rpn: 0.16375494003296s
Using PEAR RPN generic: 0.78050303459167s

RESULTS
PHP eval is slower then PHP native in 10 times
PEAR RPN true-rpn is slower then PHP native in 230 times
PEAR RPN generic is slower then PHP native in 1096 times
PEAR RPN true-rpn is slower then PHP eval in 22 times
PEAR RPN generic is slower then PHP eval in 105 times
PEAR RPN generic is slower then PEAR RPN true-rpn in 5 times

As you see, this Math_RPN thing in native RPN-mode is 22 times slower then PHP's eval() but 5 times faster itself, 
launched in generic (human native) mode.